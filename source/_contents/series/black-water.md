---
slug: black-water

title: Black Water
intro: The Ocean Will Take Us

seasons:
  1:
    publish: 2022-05-23
    interval: next friday
    announce: cada mes
    images:
      total: 38
      width: 540
      height: 1080
      icon: //img.oxkhar.com/tbo/black-water/01x:episode/black-water-t01-ep:episode.jpg
      slide: //img.oxkhar.com/tbo/black-water/01x:episode/black-water-t01-ep:episode-:image.jpg

features: [latest,carrousel,popular,essential]
genres: [thriller,drama]
authors: [rich-doueck,carlos-olivares,manuel-puppo]

byslug: "%slug%"

use: [authors_byslug]

---
Something's lurking in the waters of Almanzar Bay - and when Casey March tries out for the swim team, he learns
firsthand that messing with the social order of his new high school can have dangerous - even deadly - consequences!
A new tale of horror and intrigue - where a group of high school outcasts band together to fight a growing evil in their
school and town.
