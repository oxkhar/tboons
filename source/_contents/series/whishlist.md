---
slug: wishlist

title: Wishlist
intro: The demon who wishes you the best!

seasons:
  1:
    publish: 2022-05-23
    interval: next friday
    announce: cada mes
  2:
    publish: 2022-06-24
    interval: next monday
    announce: cada lunes

features: [latest,essential]
genres: [comedia,fantasía]
authors: [ced,stivo]

byslug: "%slug%"

use: [authors_byslug]

---
You want to be popular at school? Someone everybody knows and admires? Go from one group of friends to another? Well
then why not sell your soul to the devil, like Marcus did? Yep! It was the only way he could get his own personal demon!
Chlemnonoricon Pandaemonicus, aka Clay, can make all your wishes come true, but you only get one wish per day. Will this
help Marcus not to be a loser anymore? And more importantly: will it help him win over Jesslinda?
