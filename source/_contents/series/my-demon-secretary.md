---
slug: my-demon-secretary

title: My Demon Secretary
intro: How do you find true love in seven days?

seasons:
  1:
    publish: 2022-01-23
    interval: next friday
    announce: cada viernes


features: [latest,popular,essential]
genres: [fantasía,romance]
authors: [adeshark,ontabahlul]

byslug: "%slug%"

use: [authors_byslug]

---
Her tremendous success as the CEO of an internationally popular dating app hasn't made Grace Rosalia's life any
brighter.
Her brother's deteriorating health has her ready to take desperate measures and to do whatever it takes, no
matter how risky, to save his life.
Enter Luz, a sexy she-demon who offers to heal Grace's brother, but in exchange, Grace must help Luz find true love...
in the space of a single week! Thus begins a race against time, and a desperate one at that...
For even though Grace has designed the perfect dating app, she herself is utterly clueless when it comes to romance!
