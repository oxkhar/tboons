---
slug: die-and-retry

title: Die and retry
intro: Mortal Classroom

seasons:
  1:
    publish: 2022-02-23
    interval: next sunday
    announce: todos los lunes
    images:
      total: 38
      width: 540
      height: 1080
      icon: //img.oxkhar.com/tbo/:serie/:seasonx:episode/:serie-t:season-ep:episode.jpg
      slide: //img.oxkhar.com/tbo/:serie/:seasonx:episode/:serie-t:season-ep:episode-:image.jpg

features: [popular,latest,essential]
genres: [fantasía,romance]
authors: [franck-soullard]

byslug: "%slug%"

use: [authors_byslug]

---
Invasion zombie ! Invocation démoniaque ! Explosion nucléaire ! Pluie de météorites ! Aujourd’hui c’est la
Saint-Valentin, et le lycée est en mode Apocalypse ! Heureusement, un héros se dresse : Edward Sandgreen, loser
d’accord, mais au pouvoir immense : à chaque fois qu’il meurt, il ressuscite une heure dans le passé, à l’infirmerie.
Pour mieux recommencer… Et affronter, à la fin de cette journée complètement claquée, la plus difficile de toutes les
épreuves : embrasser la belle Pamela…
