---
slug: anchovie-akiyama

title: Anchovie Akiyama
intro: A not-so-retired detective...

seasons:
  1:
    publish: 2022-05-23
    interval: next friday
    announce: cada mes
    images:
      total: 38
      width: 540
      height: 1080
      icon: //img.oxkhar.com/tbo/die-and-retry/01x01/die-and-retry-t01-ep01.jpg
      slide: //img.oxkhar.com/tbo/die-and-retry/01x01/die-and-retry-t01-ep01-:image.jpg
  2:
    publish: 2022-06-24
    interval: next friday
    announce: cada viernes
  3:
    publish: 2022-09-14
    interval: next monday
    announce: cada lunes

features: [latest,carrousel,popular,essential]
genres: [thriller,drama]
authors: [tri-vuong]

byslug: "%slug%"

use: [authors_byslug]

---
A Tokyo detective returns to his **hometown** of St. Romaine, hoping to while away the days doing absolutely nothing.
Unfortunately for him, an evil baron, an old flame, and the town’s penchant for bizarre mysteries, means trouble is
never very far away.
