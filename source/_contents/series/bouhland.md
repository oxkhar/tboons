---
slug: bouhland

title: Bouhland  
intro: We train the crème de la crème of monsters here!

seasons:
  1:
    publish: 2022-01-23
    interval: next friday
    announce: cada viernes
  2:
    publish: 2022-04-23
    interval: next friday
    announce: cada viernes

features: [essential,carrousel,popular,latest,essential]
genres: [fantasía,aventura]
authors: [le-prince-rours]

byslug: "%slug%"

use: [authors_byslug]

---
As far back as I can remember, I've always wanted to draw.
I like illustrating both fun little comics and more serious graphic novels, usually injecting a little humor into them.
I've bounced around between gigs, from drawing to fanzines, collaborations, illustrator and art teacher before deciding
to throw myself wholeheartedly in the world of comics.
