---
slug: imaginary-daye

title: Imaginary Dave
intro: Brian's not exactly popular

seasons:
  1:
    publish: 2022-05-23
    interval: next friday
    announce: cada mes
  2:
    publish: 2022-06-24
    interval: next monday
    announce: cada lunes

features: [latest,popular,essential]
genres: [thriller,drama]
authors: [guy,teo]

byslug: "%slug%"

use: [authors_byslug]

---
A real life imaginary friend! Brian's not exactly popular. In fact, he has no friends. So why not just make one up?
Dave, his imaginary bff, can't wait to discover all that imaginary life as to offer. But when he finds out that being
imaginary means that he could stop existing at a moment's notice, he gets scared and decides that he must find a way to
become real before he disappears forever. In the process, he intends to do everything he can to help his friend Brian
get a social life.
