---
slug: rich-douek

name: Rich Douek
desc:

date: 1973-11-18

byslug: "%slug%"

---
Rich Douek is the Bram Stoker Award-nominated writer of original series like Sea of
Sorrows, Road of Bones and Wailing Blade, as well as writing for Wastelanders: Star
Lord, Superman: Red and Blue and Teenage Mutant Ninja Turtles: Universe. Born and
bred in NYC, he currently lives across the river in Montclair NJ, where he works as a
creative director in advertising when he’s not busy writing comics.
