---
slug: tri-vuong

name: Tri Vuong
desc: Pintor de brocha de gorda

date: 2002-09-21

byslug: "%slug%"

---
Tri Vuong is a comic artist and writer based out of Toronto, Canada. He is the co-creator of Skybound
Entertainment's "EVERYDAY HERO MACHINE BOY" as well as the creator of the popular webcomics "THE STRANGE TALES OF OSCAR
ZAHN" AND "ANCHOVIE AKIYAMA."  His list of clients include Ubisoft, Koei, Capybara Games, Editions Dupuis, Line Webtoon,
and Corus Entertainment.
