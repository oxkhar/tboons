---
slug: manuel-puppo

name: Manuel Puppo
desc:

date: 1978-12-08

byslug: "%slug%"

---
Manuel Puppo is an Italian colorist and illustrator. He is one of the co-founders of ARANCIA
Studio. There, he began his artistic career in the Italian market as a colorist on titles such as
Weird West Mickey and Donald Quest Saga by The Walt Disney Company. He has also
collaborated with publishers such as Le Lombard and Éditions Dupuis in Belgium, Éditions
Glénat and Petit-à-Petit in France and Egmont in Germany and Baltic countries. He departed
ARANCIA Studio in 2016, but he remains a collaborator and a friend of the Studio
