---
slug: franck-soullard

name: Franck Soullard
desc:

date: 1993-12-02

byslug: "%slug%"

---
Auteur et scénariste, Franck Soullard écrit pour la télévision (des dizaines d’épisodes de séries d’animation, concepts
de série en développement), la bande-dessinée (plusieurs séries pour Dupuis avec Webtoon Factory), et collabore à
plusieurs projets de longs-métrages. Attiré par le croisement des genres, il élabore sans cesse de nouveaux récits
originaux, destinés aux plateformes, à la BD, au roman ou
au cinéma. 
