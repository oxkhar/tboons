---
slug: camille-fourcade

name: Camille Fourcade
desc:

date: 1993-12-02

byslug: "%slug%"

---
Camille Fourcade is french illustrator born december 2, 1993 in Etampes.
Graduated from the Parisian animation school LISAA, she worked in a first time in world of animation (she was director
on the short film Waltz Duet - 2015) and then choosen to focus herself towards illustration.
After 3 years as a graphic designer in several compagnies, she decides to move into a career as a freelance illustrator
in order to fully develop her artistic universe through social networks, and give life to her characters through the
comics.
