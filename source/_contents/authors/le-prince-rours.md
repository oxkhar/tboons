---
slug: le-prince-rours

name: Le Prince Rours
desc: 

date: 2002-09-21

byslug: "%slug%"

---
As far back as I can remember, I've always wanted to draw.
I like illustrating both fun little comics and more serious graphic novels, usually injecting a little humor into them.
I've bounced around between gigs, from drawing to fanzines, collaborations, illustrator and art teacher before deciding
to throw myself wholeheartedly in the world of comics.

