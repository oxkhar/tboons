---
slug: carlos-olivares

name: Carlos J. Olivares
desc:

date: 1975-10-28

byslug: "%slug%"

---
Carlos Olivares started his professional career at the age of 15 in the Spanish market
with a series called Boumm!. Since then, he has been working in comics, animation and
gaming at Blizzard, Marvel, Z2, Soleil and Castermann. He is based in Madrid, where he
shares a studio with his agent and long-time creative collaborator, Eduardo Alpuente.
