---
slug: "%serie%/t%season%"

title: En el principio...
subtitle: Nulla consequat ac ex quis mollis

serie: bouhland
season: 1
episode: 1

images:
  total: 23
  width: 1080
  height: 2160

byseries: "%serie%"
byseasons: "%serie%/t%season%"

use: [series_byslug]
---
