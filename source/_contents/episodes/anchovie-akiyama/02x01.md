---
slug: "%serie%/t%season%"

title:  Chapter One Two
subtitle: Subtitle Chapter One

serie: anchovie-akiyama
season: 2
episode: 1

images:
  total: 23
  width: 1080
  height: 2160

byseries: "%serie%"
byseasons: "%serie%/t%season%"

use: [series_byslug]
---
