### Components config selection
##
# Docker (choose one... )
#  values: docker-compose | docker-engine (default)
MAKEUP_HAS += docker-engine
##
# Merge release (choose one... )
#  values:  git-flow | git-trunk (default)
MAKEUP_HAS += git-trunk
##
# PHP options...
#  values: php-composer php-build
#          php-qa
#          php-doctrine php-symfony
#          php-tests-xunit php-tests-mutant php-tests-rspec php-tests-bdd
MAKEUP_HAS += php-composer

### Environment configuration...
##
PHP_CLI_ENGINE = LOCAL # For local installation
# PHP_CLI_ENGINE = DOCKER # To use with PHP Docker image
# PHP_CLI_ENGINE = DOCKER_COMPOSE # Project is defined to execute in a Docker Compose file

### Tag rules to update files with new version
TAG_APP_FILES += dot.env
dot.env.tag: TAG_REG_EXP=/APP_VERSION=.*/APP_VERSION=${TAG}/

MAKEUP_HAS += js-node

###
SOURCE ?= source
BUILD  ?= build

PHP_SERVER_CLI       ?= $(PHP_DEBUG)
PHP_SERVER_HOST      ?= php-server
PHP_SERVER_PORT      ?= 8000
PHP_SERVER_PORT_HOST ?= ${PHP_SERVER_PORT}
PHP_SERVER_ROOT      ?= ${BUILD}

PHP_SERVER = ${PHP_SERVER_CLI} -S 0.0.0.0:${PHP_SERVER_PORT} -t ${PHP_SERVER_ROOT}

APP_TARGETS += web design start stop web
APP_TARGETS += php-server php-server-stop php-server-logs
APP_TARGETS += watch watch-changes

php-server: DOCKER_RUN_OPTIONS+= --detach --name ${PHP_SERVER_HOST} \
								 --publish ${PHP_SERVER_PORT_HOST}:${PHP_SERVER_PORT}
php-server-logs:
	$I View logs "(Ctrl+C to stop)"
	${DOCKER_BIN} container logs -f ${PHP_SERVER_HOST}

php-server-stop:
	$I Stoping web server
	${DOCKER_BIN} container stop ${PHP_SERVER_HOST}

php-server:
	$I Starting web server
	${PHP_SERVER}

watch-changes:
	watch -d -g -x du -bs --time --time-style=full-iso ${SOURCE}

watch: watch-changes web   ## Watch changes in source paths to build project
	make watch

start: php-server web      ## Prepare and start project to develop
	make watch

stop: php-server-stop        ## Prepare and start project to develop

web:                       ## Build web app
	$I Building web
	$I Generate contents
	$(PHP_CLI) ${BIN}/sculpin generate --no-interaction --clean --output-dir ${BUILD} --source-dir ${SOURCE} || exit $?
	$I Compile app
	$(NPM) run build || exit $?

###
include make/MakeUp.mk
