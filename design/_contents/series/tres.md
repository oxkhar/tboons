---
slug: velit

title: Quisque velit orci
desc: Nulla blandit lorem quis consequat porttitor
date: 2005-09-21

features: [essential]
genres: [comedia, romance]

byslug: "%slug%"

---
Pellentesque dignissim urna et mi convallis scelerisque. Quisque dui enim, elementum vel vulputate
congue, rhoncus sed nunc. Aliquam mi erat, imperdiet nec quam et, consectetur sollicitudin dolor.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In
cursus, justo nec pellentesque posuere, massa sem aliquet nulla, at faucibus risus sem porta arcu.
Aenean turpis augue, **suscipit** et efficitur ac, ultricies vel metus. Nulla blandit lorem quis
consequat porttitor. Maecenas sollicitudin elit sed dignissim imperdiet. Lorem ipsum dolor sit amet,
consectetur adipiscing elit. Quisque velit orci, efficitur a iaculis quis, tempus tincidunt est.
Praesent sem orci, varius ac nisl quis, vehicula interdum purus. 
