const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
    "./source/**/*.{html,js,ts,jsx,tsx,twig}",
    "./design/**/*.{html,js,ts,jsx,tsx,twig}",
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [
    require('@tailwindcss/aspect-ratio'),
  ],
}
